
#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe {
private:
	char m_board[9] = { 1,2,3,4,5,6,7,8,9 };
	int m_numTurns;
	char m_winner;
	static const char p1 = 1;
	static const char p2 = 2;
	char m_playerTurn = p1;

public:
	void DisplayBoard() {
		m_numTurns += 1;
		cout << m_board[0] << "|" << m_board[1] << "|" << m_board[2] << endl;
		cout << "------" << endl;
		cout << m_board[3] << "|" << m_board[4] << "|" << m_board[5] << endl;
		cout << "------" << endl;
		cout << m_board[6] << "|" << m_board[7] << "|" << m_board[8] << endl;
	}
	char GetPlayerTurn() {
		if (m_playerTurn == p1)
		{
			return 'X';
		}
		else
		{
			return 'O';
		}
	}
	bool IsValidMove(int pos) {
		if (m_board[pos] == 'X' | m_board[pos] == 'O')
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	void Move(int pos) {
		m_board[pos - 1] = GetPlayerTurn();
		if (m_playerTurn == p1)
		{
			m_playerTurn = p2;
		}
		else
		{
			m_playerTurn = p1;
		}
	}
	bool IsOver() {
		if (m_numTurns == 9)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	void DisplayResult() {
		cout << "The game is over.\n";
		
	}
};